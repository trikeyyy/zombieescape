package ch.trikeyyy.mcze.profiles;

import ch.trikeyyy.mcze.core.constants.Achievements;
import ch.trikeyyy.mcze.core.constants.KitType;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

import java.util.UUID;

@Getter
@Setter
public class Profile {

    private UUID uuid;
    private String name;

    private int zombieKills;
    private int humanKills;
    private int points;
    private int wins;
    private int gamesPlayed;

    private boolean loaded;

    private char[] achievements;

    private KitType humanKit;
    private KitType zombieKit;

    public Profile(Player player) {
        this.uuid = player.getUniqueId();
        this.name = player.getName();
    }

    public void awardAchievement(Achievements achievement) {
//        achievements[achievement.getId()] = 't';
    }

    public int getHumanKills() {
        return humanKills;
    }

    public void setHumanKills(int humanKills) {
        this.humanKills = humanKills;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public void setZombieKills(int zombieKills) {
        this.zombieKills = zombieKills;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public void setAchievements(char[] achievements) {
        this.achievements = achievements;
    }

    public void setHumanKit(KitType humanKit) {
        this.humanKit = humanKit;
    }

    public void setZombieKit(KitType zombieKit) {
        this.zombieKit = zombieKit;
    }


    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }
}