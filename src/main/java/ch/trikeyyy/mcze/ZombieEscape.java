package ch.trikeyyy.mcze;

import ch.trikeyyy.mcze.core.GameManager;
import ch.trikeyyy.mcze.guis.HumanKitMenu;
import ch.trikeyyy.mcze.guis.ZombieKitMenu;
import ch.trikeyyy.mcze.listeners.*;
import ch.trikeyyy.mcze.utils.menus.MenuManager;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import ch.trikeyyy.mcze.core.GameArena;
import ch.trikeyyy.mcze.utils.Configuration;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class ZombieEscape extends JavaPlugin {

    private GameArena gameArena;
    private HikariDataSource hikari;
    private GameManager gameManager;
    private MenuManager menuManager;
    private Configuration configuration;

    @Override
    public void onEnable() {
        // setupHikari();

        gameArena = new GameArena(this);
        gameManager = new GameManager();
        menuManager = new MenuManager(this);
        configuration = new Configuration(this);

        menuManager.addMenu("hkits", new HumanKitMenu("Human Kit Menu", 9) {
            @Override
            public Inventory getInventory() {
                return null;
            }
        });
        menuManager.addMenu("zkits", new ZombieKitMenu("Zombie Kit Menu", 9) {
            @Override
            public Inventory getInventory() {
                return null;
            }
        });

        registerListeners();
    }

    // TODO: Cleanup open files/instances/etc
    @Override
    public void onDisable() {
        if (hikari != null) {
            hikari.close();
        }
    }

    private void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new EntityDamageByEntity(this), this);
        pm.registerEvents(new FoodLevelChange(), this);
        pm.registerEvents(new PlayerDeath(this), this);
        pm.registerEvents(new PlayerDropItem(), this);
        pm.registerEvents(new PlayerInteract(this), this);
        pm.registerEvents(new PlayerJoin(this), this);
        pm.registerEvents(new PlayerPickupItem(), this);
        pm.registerEvents(new PlayerQuit(this), this);
    }

    private void setupHikari() {
        FileConfiguration config = configuration.getSettingsConfig();

        String address = config.getString("Database.Address");
        String name = config.getString("Database.Schema");
        String username = config.getString("Database.Username");
        String password = config.getString("Database.Password");

        hikari = new HikariDataSource();
        hikari.setMaximumPoolSize(10);
        hikari.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        hikari.addDataSourceProperty("serverName", address.split(":")[0]);
        hikari.addDataSourceProperty("port", address.split(":")[1]);
        hikari.addDataSourceProperty("databaseName", name);
        hikari.addDataSourceProperty("user", username);
        hikari.addDataSourceProperty("password", password);
    }

    public GameArena getGameArena() {
        return gameArena;
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public HikariDataSource getHikari() {
        return hikari;
    }
}