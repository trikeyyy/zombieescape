package ch.trikeyyy.mcze.core.constants;

import ch.trikeyyy.mcze.core.kitcallbacks.Leaper;
import ch.trikeyyy.mcze.core.kitcallbacks.Tank;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum KitType {

    LEAPER("Leaper", new Leaper()),

    TANK("Tank", new Tank());

    private String name;
    private KitAction kitAction;

}