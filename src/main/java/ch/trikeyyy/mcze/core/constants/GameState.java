package ch.trikeyyy.mcze.core.constants;

public enum GameState {
    WAITING, STARTING, RUNNING, RESTRICTED
}