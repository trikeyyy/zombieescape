package ch.trikeyyy.mcze.core;

import lombok.Getter;
import ch.trikeyyy.mcze.profiles.Profile;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Getter
public class GameManager {

    private Map<UUID, Profile> profiles = new HashMap<>();

    public Profile getProfile(Player player) {
        return profiles.get(player.getUniqueId());
    }

    public Profile getRemovedProfile(Player player) {
        return profiles.remove(player.getUniqueId());
    }

    public Map<UUID, Profile> getProfiles() {
        return profiles;
    }
}