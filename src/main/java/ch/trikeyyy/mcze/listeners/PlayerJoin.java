package ch.trikeyyy.mcze.listeners;

import ch.trikeyyy.mcze.ZombieEscape;
import ch.trikeyyy.mcze.core.GameArena;
import ch.trikeyyy.mcze.profiles.Profile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {

    private final ZombieEscape PLUGIN;

    public PlayerJoin(ZombieEscape plugin) {
        this.PLUGIN = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        GameArena gameArena = PLUGIN.getGameArena();

        if (gameArena.isGameRunning()) {

        } else {
            if (gameArena.shouldStart()) {
                gameArena.startCountdown();
            }
        }

        final Profile PROFILE = new Profile(event.getPlayer());
        PLUGIN.getGameManager().getProfiles().put(event.getPlayer().getUniqueId(), PROFILE);
        //new ProfileLoader(PROFILE, PLUGIN).runTaskAsynchronously(PLUGIN);
    }

}