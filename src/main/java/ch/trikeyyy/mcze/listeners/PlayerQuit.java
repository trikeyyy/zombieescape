package ch.trikeyyy.mcze.listeners;

import ch.trikeyyy.mcze.ZombieEscape;
import ch.trikeyyy.mcze.core.GameArena;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuit implements Listener {

    private final ZombieEscape PLUGIN;

    public PlayerQuit(ZombieEscape plugin) {
        this.PLUGIN = plugin;
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        GameArena gameArena = PLUGIN.getGameArena();

        gameArena.purgePlayer(event.getPlayer());

        if (gameArena.isGameRunning()) {
            if (gameArena.shouldEnd()) {
                // Possibly deduct points?
                gameArena.endGame();
            }
        }

        //Profile profile = PLUGIN.getGameManager().getRemovedProfile(event.getPlayer());
        //new ProfileSaver(profile, PLUGIN).runTaskAsynchronously(PLUGIN);
    }

}